//! main entry
//!

extern crate leetcode;

fn main() {
    println!(
        "leetcode->two_sum-> {:?}",
        leetcode::two_sum::indices(vec![2, 7, 11, 15], 9)
    );

    println!(
        "leetcode->add_two_numbers-> {:?}",
        leetcode::add_two_numbers::add(vec![9, 9, 9, 9, 9, 9, 9], vec![9, 9, 9, 9])
    );

    println!(
        "leetcode->longest_substring_without_repeating_chars-> {:?}",
        leetcode::longest_substring_without_repeating_chars::length("QWErtYou_dd dre")
    );

    println!(
        "leetcode->valid_parentheses-> {:?}",
        leetcode::valid_parentheses::is_valid("{[()}]")
    );

    println!(
        "leetcode->reverse_integer-> {:?}",
        leetcode::reverse_integer::value(911)
    );

    println!(
        "leetcode->find_first_last-> {:?}",
        leetcode::find_first_last::find(&vec![5, 7, 7, 8, 8, 10], 8)
    );
}
