# Add Two Numbers
You are given two **non-empty** linked lists representing two non-negative integers. The digits are stored in **reverse** order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.
* Example 1:
```
Input: l1 = [2,4,3], l2 = [5,6,4]
Output: [7,0,8]
Explanation: 342 + 465 = 807.
```
* Example 2:
```
Input: l1 = [0], l2 = [0]
Output: [0]
```
* Example 3:
```
Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
Output: [8,9,9,9,0,0,0,1]
```
* Constraints:
    - The number of nodes in each linked list is in the range [1, 100].
    - 0 <= Node.Val <= 9
    - It is guaranteed that the list represents a number that does not have leading zeros.

# Find First and Last Position of Element in Sorted Array
Given an array of integers **nums** sorted in ascending order, find the starting and ending position of a given **target** value.

If **target** is not found in the array, return [-1, -1].

You must write an algorithm with O(log n) runtime complexity.

* Example 1:
```
Input: nums = [5,7,7,8,8,10], target = 8
Output: [3,4]
```
* Example 2:
```
Input: nums = [5,7,7,8,8,10], target = 6
Output: [-1,-1]
```
* Example 3:
```
Input: nums = [], target = 0
Output: [-1,-1]
```
* Constraints:
    - 0 <= nums.length <= $10^{5}$
    - -$10^{9}$ <= nums[i] <= $10^{9}$
    - nums is a non-decreasing array.
    - -$10^{9}$ <= target <= $10^{9}$

# Longest Substring Without Repeating Characters
Given a string s, find the length of the **longest substring** without repeating characters.

* Example 1:
```
Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.
```
* Example 2:
```
Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.
```
* Example 3:
```
Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.
```
* Example 4:
```
Input: s = ""
Output: 0
```
* Constraints:
    - 0 <= s.length <= 5 * $10^4$
    - s consists of English letters, digits, symbols and spaces.

# Merge Two Sorted Lists
Merge two sorted linked lists and return it as a **sorted** list. The list should be made by splicing together the nodes of the first two lists.

* Example 1:
```
Input: l1 = [1,2,4], l2 = [1,3,4]
Output: [1,1,2,3,4,4]
```
* Example 2:
```
Input: l1 = [], l2 = []
Output: []
```
* Example 3:
```
Input: l1 = [], l2 = [0]
Output: [0]
```
* Constraints:
    - The number of nodes in both lists is in the range [0, 50].
    - -100 <= Node.val <= 100
    - Both l1 and l2 are sorted in non-decreasing order.

# Reverse Integer
Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-2$^{31}$, 2$^{31}$ - 1], then return 0.

Assume the environment does not allow you to store 64-bit integers (signed or unsigned).

* Example 1:
```
Input: x = 123
Output: 321
Explanation: The answer is "abc", with the length of 3.
```
* Example 2:
```
Input: x = -123
Output: -321
```
* Example 3:
```
Input: x = 120
Output: 21
```
* Example 4:
```
Input: x = 0
Output: 0
```
* Constraints:
    - -$2^{31}$ <= x <= $2^{31}$ - 1

# Two Sum
Given an array of integers **nums** and an integer **target**, return indices of the two numbers such that they add up to target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

You can return the answer in any order.

* Example 1:
```
Input: nums = [2,7,11,15], target = 9
Output: [0,1]
Output: Because nums[0] + nums[1] == 9, we return [0, 1].
```
* Example 2:
```
Input: nums = [3,2,4], target = 6
Output: [1,2]
```
* Example 3:
```
Input: nums = [3,3], target = 6
Output: [0,1]
```
* Constraints:
    - 2 <= nums.length <= $10^4$
    - -$10^9$ <= nums[i] <= $10^9$
    - -$10^9$ <= target <= $10^9$
    - Only one valid answer exists.

# Valid Parentheses
Given a string **s** containing just the characters **'('**, **')'**, **'{'**, **'}'**, **'['** and **']'**, determine if the input string is valid.

An input string is valid if:

    1. Open brackets must be closed by the same type of brackets.
    2. Open brackets must be closed in the correct order.

* Example 1:
```
Input: s = "()"
Output: true
```
* Example 2:
```
Input: s = "()[]{}"
Output: true
```
* Example 3:
```
Input: s = "(]"
Output: false
```
* Example 4:
```
Input: s = "([)]"
Output: false
```
* Example 5:
```
Input: s = "{[]}"
Output: true
```
* Constraints:
    - 1 <= s.length <= $10^4$
    - **s** consists of parentheses only **'()[]{}'**