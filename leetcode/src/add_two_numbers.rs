pub type List = Option<Box<ListNode>>;

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: u32,
    pub next: List,
}

impl ListNode {
    #[inline]
    fn new(val: u32) -> Self {
        ListNode { next: None, val }
    }
}

fn vec_to_list(mut value: Vec<u32>) -> List {
    match value.pop() {
        Some(x) => Some(Box::new(ListNode {
            val: x,
            next: vec_to_list(value),
        })),
        None => None,
    }
}

#[allow(dead_code)]
fn vec_to_list_iter(mut value: Vec<u32>) -> List {
    let mut head = None;
    let mut tail = &mut head;

    for val in value.iter_mut() {
        *tail = Some(Box::new(ListNode::new(*val)));
        tail = &mut tail.as_mut().unwrap().next;
    }

    head
}

#[allow(dead_code)]
fn vec_to_list_iter_mut_ref(value: &mut Vec<u32>) -> List {
    let mut dummy = ListNode::new(0);
    let mut tail = &mut dummy;

    for val in value.iter_mut() {
        tail.next = Some(Box::new(ListNode::new(*val)));
        tail = tail.next.as_mut().unwrap();
    }

    dummy.next
}

fn list_to_vec(mut value: &List) -> Vec<u32> {
    let mut v = Vec::new();
    while let Some(n) = value {
        v.push(n.val);
        value = &n.next;
    }
    v
}

/// Add the two numbers and return the sum as a linked list.
///
/// # Examples
///
/// ```
/// extern crate leetcode;
/// use leetcode::add_two_numbers::add;
/// let result = add(vec![2, 4, 3], vec![5, 6, 4]);
/// ```
pub fn add(l1: Vec<u32>, l2: Vec<u32>) -> Vec<u32> {
    let mut carry = 0;
    let mut head: List = None;
    let mut tail = &mut head;

    let (mut p, mut q) = (&vec_to_list(l1), &vec_to_list(l2));
    while p.is_some() || q.is_some() || carry != 0 {
        let mut sum = carry;
        if let Some(node) = p {
            sum += node.val;
            p = &node.next;
        }

        if let Some(node) = q {
            sum += node.val;
            q = &node.next;
        }

        carry = sum / 10;
        *tail = Some(Box::new(ListNode::new(sum % 10)));
        tail = &mut tail.as_mut().unwrap().next;
    }

    list_to_vec(&head)
}

#[cfg(test)]
mod tests {
    use super::*;

    mod add {
        use super::*;

        #[test]
        fn test() {
            let result = add(vec![2, 4, 3], vec![5, 6, 4]);
            assert_eq!(result, [7, 0, 8]);
        }
    }
}
