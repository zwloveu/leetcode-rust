/// return x with its digits reversed
///
/// # Examples
///
/// ```
/// extern crate leetcode;
/// use leetcode::reverse_integer::value;
/// let result = value(911);
/// ```
pub fn value(x: i32) -> i32 {
    if x == 0 {
        return x;
    }

    let value_to_parse = if x < 0 { -x } else { x };
    let value = match value_to_parse
        .to_string()
        .chars()
        .rev()
        .collect::<String>()
        .parse::<i32>()
    {
        Ok(value) => value,
        _ => 0,
    };

    if x < 0 {
        -value
    } else {
        value
    }
}

#[cfg(test)]
mod tests {
    mod value {
        use crate::reverse_integer::*;

        #[test]
        fn test() {
            let result = value(911);
            assert_eq!(result, 119);
        }
    }
}
