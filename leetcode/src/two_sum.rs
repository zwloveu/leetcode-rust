use std::collections::HashMap;

/// Return indices of the two numbers such that they add up to target.
///
/// # Examples
///
/// ```
/// extern crate leetcode;
/// use leetcode::two_sum::indices;
/// let result: std::vec::Vec<i32> = indices(vec![2, 7, 11, 15], 9);
/// ```
pub fn indices(nums: Vec<i32>, target: i32) -> Vec<i32> {
    let mut map: HashMap<i32, i32> = HashMap::new();
    for (i, num) in nums.iter().enumerate() {
        let complement = target - num;
        if map.contains_key(&complement) {
            return vec![map[&complement], i as i32];
        }
        map.insert(*num, i as i32);
    }
    vec![]
}

#[cfg(test)]
mod tests {
    mod indices {
        use crate::two_sum::*;

        #[test]
        fn test() {
            let nums = vec![2, 7, 11, 15];
            let target = 9;
            let result = indices(nums, target);
            assert_eq!(result, [0, 1]);

            let nums = vec![3, 2, 4];
            let target = 6;
            let result = indices(nums, target);
            assert_eq!(result, vec![1, 2]);

            let nums = vec![3, 3];
            let target = 6;
            let result = indices(nums, target);
            assert_eq!(result, vec![0, 1]);
        }
    }
}
