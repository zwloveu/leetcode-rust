/// Return the starting and ending position of a given target value.
///
/// # Examples
///
/// ```
/// extern crate leetcode;
/// use leetcode::find_first_last::find;
/// let result = find(&vec![5,7,7,8,8,10], 8);
/// ```
pub fn find(nums: &Vec<i32>, target: i32) -> Vec<i32> {
    match nums.len() {
        0 => vec![-1, -1],
        _ => {
            let (mut start, mut end) = (-1, -1);
            for i in 0..nums.len() {
                if nums[i] != target {
                    continue;
                }
                if start == -1 {
                    start = i as i32;
                    end = i as i32;
                } else {
                    end = i as i32;
                }
            }
            vec![start, end]
        }
    }
}

#[cfg(test)]
mod tests {
    mod find {
        use crate::find_first_last::*;

        #[test]
        fn test() {
            let result = find(&vec![5, 7, 7, 8, 8, 10], 8);
            assert_eq!(result, [3, 4]);

            let result = find(&vec![5, 7, 7, 8, 8, 10], 6);
            assert_eq!(result, [-1, -1]);
        }
    }
}
