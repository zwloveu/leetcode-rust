use std::cmp::max;
use std::collections::HashMap;

/// Return the length of the **longest substring** without repeating characters.
///
/// # Examples
///
/// ```
/// extern crate leetcode;
/// use leetcode::longest_substring_without_repeating_chars::length;
/// let result = length("abcabcbb");
/// ```
pub fn length(s: &str) -> i32 {
    let mut m = HashMap::new();
    let mut ans = 0;
    let mut before = -1;
    let mut current = 0;
    for c in s.chars() {
        if let Some(last) = m.insert(c, current) {
            before = max(before, last);
        }
        ans = max(ans, current - before);
        current += 1;
    }
    ans
}

#[cfg(test)]
mod tests {
    mod length {
        use crate::longest_substring_without_repeating_chars::*;

        #[test]
        fn test() {
            let result = length("abcabcbb");
            assert_eq!(result, 3);
        }
    }
}
