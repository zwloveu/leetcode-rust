/// Determine if the input string is valid:
/// Open brackets must be closed by the same type of brackets.
/// Open brackets must be closed in the correct order.
///
/// # Examples
///
/// ```
/// extern crate leetcode;
/// use leetcode::valid_parentheses::is_valid;
/// let result = is_valid("{[(]})");
/// ```
pub fn is_valid(s: &str) -> bool {
    let mut stack = Vec::new();
    for i in s.chars() {
        match i {
            '{' => stack.push('}'),
            '(' => stack.push(')'),
            '[' => stack.push(']'),
            '}' | ')' | ']' if Some(i) != stack.pop() => return false,
            _ => (),
        }
    }
    return stack.is_empty();
}

#[cfg(test)]
mod tests {
    mod is_valid {
        use crate::valid_parentheses::*;

        #[test]
        fn test() {
            let result = is_valid("()[]{}");
            assert_eq!(result, true);
        }
    }
}
